Source: rust-condure
Section: net
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native,
 rustc:native,
 libstd-rust-dev,
 librust-arrayvec-0.7+default-dev,
 librust-base64-0.13+default-dev | librust-base64-0.12+default-dev,
 librust-clap-2+default-dev (>= 2.33-~~),
 librust-clap-2+wrap-help-dev (>= 2.33-~~),
 librust-httparse-1+default-dev (>= 1.3-~~),
 librust-log-0.4+default-dev,
 librust-mio-0.8+default-dev,
 librust-mio-0.8+net-dev,
 librust-mio-0.8+os-ext-dev,
 librust-mio-0.8+os-poll-dev,
 librust-openssl-0.10+default-dev,
 librust-paste-1+default-dev,
 librust-sha1-0.10+default-dev,
 librust-signal-hook-0.3+default-dev,
 librust-slab-0.4+default-dev,
 librust-socket2-0.4+default-dev,
 librust-time-0.1+default-dev,
 librust-zmq-0.9+default-dev,
 help2man
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Jan Niehusmann <jan@debian.org>
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/condure]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/condure
X-Cargo-Crate: condure
Rules-Requires-Root: no

Package: condure
Architecture: any
Multi-Arch: allowed
Section: net
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
XB-X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: HTTP/WebSocket connection manager
 Condure is a service that manages network connections on behalf of server
 applications, in order to allow controlling the connections from multiple
 processes. Applications communicate with Condure over ZeroMQ.
 .
 Condure can only manage connections for protocols it knows
 about. Currently this is HTTP/1 and WebSockets. See Supported protocols.
 .
 The project was inspired by Mongrel2.
