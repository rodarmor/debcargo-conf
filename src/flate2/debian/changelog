rust-flate2 (1.0.24-2) unstable; urgency=medium

  * Team upload.
  * Package flate2 1.0.24 from crates.io using debcargo 2.5.0
  * Upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 18 Oct 2022 15:52:15 +0000

rust-flate2 (1.0.24-1) experimental; urgency=medium

  * Team upload.
  * Package flate2 1.0.24 from crates.io using debcargo 2.5.0

 -- Fabian Grünbichler <f.gruenbichler@proxmox.com>  Tue, 27 Sep 2022 20:31:30 -0400

rust-flate2 (1.0.22-1) unstable; urgency=medium

  * Team upload.
  * Package flate2 1.0.22 from crates.io using debcargo 2.4.4
  * Disable cloudflare zlib support, we don't have cloudflare zlib
    in Debian.
  * Disable zlib-ng support, we don't have zlib-ng in Debian.
  * Eliminate internal "any_zlib" feature to prevent debcargo
    from splitting libz-sys and zlib feature packages.

  [ Fabian Grünbichler ]
  * Patch out futures and tokio features, they are not yet compatible with
    futures 0.3 / tokio 0.2 and have no rdeps

 -- Peter Michael Green <plugwash@debian.org>  Thu, 21 Oct 2021 17:40:04 +0000

rust-flate2 (1.0.13-3) unstable; urgency=medium

  * Team upload.
  * Package flate2 1.0.13 from crates.io using debcargo 2.4.3
  * Set the test_is_broken flag, the no features test is broken and the
    autopkgtest as whole has never passed (until recently it was suppressed
    due to uninstallable test dependencies) (Closes: 980193)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 23 Jan 2021 15:27:27 +0000

rust-flate2 (1.0.13-2) unstable; urgency=medium

  * Team upload.
  * Package flate2 1.0.13 from crates.io using debcargo 2.4.2
  * Source-only upload for testing migration.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 13 Apr 2020 18:00:52 +0000

rust-flate2 (1.0.13-1) unstable; urgency=medium

  * Team upload.
  * Package flate2 1.0.13 from crates.io using debcargo 2.4.0

 -- Ximin Luo <infinity0@debian.org>  Thu, 28 Nov 2019 01:46:12 +0000

rust-flate2 (1.0.9-1) unstable; urgency=medium

  * Package flate2 1.0.9 from crates.io using debcargo 2.2.10
  * Refresh disable-miniz.patch

 -- Paride Legovini <pl@ninthfloor.org>  Sun, 14 Jul 2019 15:23:56 +0000

rust-flate2 (1.0.6-1) unstable; urgency=medium

  [ Ximin Luo ]
  * Team upload.
  * Package flate2 1.0.6 from crates.io using debcargo 2.2.9

 -- Matt Kraai <kraai@debian.org>  Sun, 09 Dec 2018 21:06:30 -0800

rust-flate2 (1.0.2-1) unstable; urgency=medium

  * Package flate2 1.0.2 from crates.io using debcargo 2.2.6
  * Disable the actual miniz implementation and have it transparently use
    zlib instead.

  [ Paride Legovini ]
  * Package flate2 1.0.1 from crates.io using debcargo 2.2.3

 -- Ximin Luo <infinity0@debian.org>  Sun, 26 Aug 2018 10:23:08 +0200
